//
//  API.swift
//
//

import Foundation
import SwiftyJSON






typealias JSONDictionary = Dictionary<String, AnyObject>
typealias JSONArray = Array<AnyObject>

class API: NSObject, NSURLConnectionDataDelegate {
    
    enum Path {
        case SIGIN_IN
        case GET_EVENTS
    }
    
    typealias APICallback = ((JSON, NSError?) -> ())
    let responseData = NSMutableData()
    var statusCode:Int = -1
    var callback: APICallback! = nil
    var path: Path! = nil
    
    
    func signIn(email: String, password: String, callback: APICallback) {
        
        
        var uuid = NSUUID().UUIDString
        let deviceid = "1";
        let brand = "6"
        //let url = "\(Config.baseURL())/api/sessions.json"
        let url = "https://www.flavorus.com/API.aspx"
        
        let body = "email=\(email)&hashedpass=\(password.stringByAddingPercentEscapesForQueryValue()!)&deviceid=\(deviceid)&devicetype=ios&salt=\(uuid)&call=SignIn&brand=\(brand)"
        
        makeHTTPPostRequest(Path.SIGIN_IN, callback: callback, url: url, body: body)
    }
    
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        let httpResponse = response as! NSHTTPURLResponse
        statusCode = httpResponse.statusCode
        switch (httpResponse.statusCode) {
        case 201, 200, 401:
            self.responseData.length = 0
        default:
            println("ignore")
        }
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        self.responseData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        var error: NSError?
        var json : AnyObject! = NSJSONSerialization.JSONObjectWithData(self.responseData, options: NSJSONReadingOptions.MutableLeaves, error: &error)
        if (error != nil) {
            callback(nil, error)
            return
        }
        
        switch(statusCode, self.path!) {
        case (200, Path.SIGIN_IN):
            callback(self.handleSignIn(json), nil)
        case (401, _):
            callback(nil, handleAuthError(json))
        default:
            // Unknown Error
            callback(nil, nil)
        }
    }
    
    func handleAuthError(json: AnyObject) -> NSError {
        if let resultObj = json as? JSONDictionary {
            // beta2 workaround
            if let messageObj: AnyObject = resultObj["error"] {
                if let message = messageObj as? String {
                    return NSError(domain:"signIn", code:401, userInfo:["error": message])
                }
            }
        }
        return NSError(domain:"signIn", code:401, userInfo:["error": "unknown auth error"])
    }
    
    func handleSignIn(json: AnyObject) -> JSON {
        return JSON(json)
    }
    
    // private
    func makeHTTPGetRequest(path: Path, callback: APICallback, url: NSString) {
        self.path = path
        self.callback = callback
        let request = NSURLRequest(URL: NSURL(string: url as String)!)
        let conn = NSURLConnection(request: request, delegate:self)
        if (conn == nil) {
            callback(nil, nil)
        }
    }
    
    func makeHTTPPostRequest(path: Path, callback: APICallback, url: NSString, body: NSString) {
        self.path = path
        self.callback = callback
        let request = NSMutableURLRequest(URL: NSURL(string: url as String)!)
        request.HTTPMethod = "POST"
        request.HTTPBody = body.dataUsingEncoding(NSUTF8StringEncoding)
        let conn = NSURLConnection(request: request, delegate:self)
        if (conn == nil) {
            callback(nil, nil)
        }
    }
    
    /*func sha256(data : NSData) -> NSData {
        var hash = [UInt8](count: Int(CC_SHA256_DIGEST_LENGTH), repeatedValue: 0)
        CC_SHA256(data.bytes, CC_LONG(data.length), &hash)
        let res = NSData(bytes: hash, length: Int(CC_SHA256_DIGEST_LENGTH))
        return res
    }*/
}

extension String {
    
    /// Percent escape value to be added to a URL query value as specified in RFC 3986
    ///
    /// This percent-escapes all characters except the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: Return precent escaped string.
    
    func stringByAddingPercentEscapesForQueryValue() -> String? {
        let characterSet = NSMutableCharacterSet.alphanumericCharacterSet()
        characterSet.addCharactersInString("-._~")
        return stringByAddingPercentEncodingWithAllowedCharacters(characterSet)
    }
}

