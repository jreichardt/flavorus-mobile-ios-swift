//
//  LoginViewController.swift
//  Ticketon Box Office
//
//  Created by admin on 4/28/15.
//  Copyright (c) 2015 Ticketon. All rights reserved.
//

import UIKit
import CryptoSwift

class LoginViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBAction func signInButtonPushed(sender: AnyObject) {
        //let passwordstr = "iGoBgj7dqMLfAyV+8nKlj+hySImx55WDonfa5PZ/XjM="
        
        var email:String = txtEmail.text as String
        var password:String = txtPassword.text as String

        var hash2 = password.sha256()
        let utf16str = password.dataUsingEncoding(NSUTF16StringEncoding)
        var utf8stra = password.dataUsingEncoding(NSUTF8StringEncoding)
        let hashedpassword = CryptoSwift.Hash.sha256(utf16str!).calculate()
                let hashedpassword2 = CryptoSwift.Hash.sha256(utf8stra!).calculate()
        let base = hashedpassword!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        if let base64Encoded = hashedpassword?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        {
            if (true){
                var api = API()
                api.signIn(email, password: base64Encoded, callback: { (data, error) in
                    if (error == nil) {
                        // do stuff with data
                        if (data["success"] == false)
                        {
                            let messagestr = data["message"].stringValue
                            
                            var alert = UIAlertController(title: "Error", message: messagestr, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "close", style: UIAlertActionStyle.Default, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                            
                            
                        }else{
                            let name = data["Name"].stringValue
                            
                            var alert = UIAlertController(title: "Success", message: name, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "close", style: UIAlertActionStyle.Default, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                            
                            
                        }
                    }
                })
                
            }
        }
        
        
    }
    
    
    
}
